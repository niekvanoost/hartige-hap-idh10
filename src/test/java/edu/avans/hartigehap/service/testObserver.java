package edu.avans.hartigehap.service;

import org.junit.Test;

import edu.avans.hartigehap.domain.PaidOrder;
import edu.avans.hartigehap.repository.Observer;
import edu.avans.hartigehap.service.impl.SubjectImpl;

public class testObserver {
    @Test
    public void observerTest() throws Exception {
        Subject product=new SubjectImpl();
        Observer kitchen=new PaidOrder("Kitchen");
        product.registerObserver(kitchen);
        Observer waiter=new PaidOrder("Waiter");
        product.registerObserver(waiter);
        product.setMessageVars(40);
    }
}