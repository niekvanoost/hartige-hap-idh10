package edu.avans.hartigehap.service;

import java.util.Collection;

import edu.avans.hartigehap.domain.*;
import edu.avans.hartigehap.repository.BaseOrderItemRepository;

public interface BaseOrderItemService {
    
    void save(OrderOption orderOption);
    void saveItem(OrderItem orderItem);
    
    BaseOrderItem findOne(Long id);
    
    void addOrder(DiningTable diningTable, BaseOrderItem orderItem);

	void removeOrder(DiningTable diningTable);
	
	void addOption(DiningTable diningTable, String menuItemName);
	
	void removeOption(DiningTable diningTable, String menuItemName);
	
	Long getLastBaseOrderItemID();

	BaseOrderItem getBaseOrderItem(Long menuItemName);
	
	Long getLastOrderOptionIdByMeal(String menuItemName);
	
	Collection<BaseOrderItem> getAllBaseOrderItems();
    
}