package edu.avans.hartigehap.service.impl;

import java.util.Arrays;
import java.util.List;
import java.util.ListIterator;
import java.util.Iterator;
import java.util.Collection;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import edu.avans.hartigehap.domain.*;
import edu.avans.hartigehap.repository.*;
import edu.avans.hartigehap.service.*;

import com.google.common.collect.Lists;

@Service("InternetCustomerService")
@Repository
@Transactional
public class InternetCustomerServiceImpl implements InternetCustomerService {
	
	@Autowired
	private InternetCustomerRepository internetCustomerRepostory;

	public void save(RealCustomer customer){
		internetCustomerRepostory.save(customer);

	}

}
