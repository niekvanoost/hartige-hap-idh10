package edu.avans.hartigehap.service.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import edu.avans.hartigehap.repository.*;
import edu.avans.hartigehap.service.*;
import edu.avans.hartigehap.domain.*;

@Service("billService")
@Repository
@Transactional(rollbackFor = StateException.class)
public class BillServiceImpl implements BillService {

    @Autowired
    private OrderRepository orderRepository;
    @Autowired
    private BillRepository billRepository;

    @Transactional(readOnly = true)
    public Bill findById(Long billId) {
        return billRepository.findOne(billId);
    }

    public void billHasBeenPaid(Bill bill) throws StateException {
        bill.paid();
    }


    @Transactional(readOnly = true)
    public List<Bill> findSubmittedBillsForRestaurant(Restaurant restaurant) {
        // a query created using a repository method name
        return billRepository.findByBillStatusAndDiningTableRestaurant(
                Bill.BillStatus.SUBMITTED, restaurant, new Sort(Sort.Direction.ASC, "submittedTime"));
    }
    public List<Bill> findPaidBillsForRestaurant (Restaurant restaurant){
    	return billRepository.findByBillStatusAndDiningTableRestaurant(Bill.BillStatus.PAID, restaurant, new Sort(Sort.Direction.ASC, "submittedTime"));
    }
    
    public Order getOrder(Bill bill){
    	return bill.getCurrentOrder();
    }
    
    public Collection<Order> getSubmittedOrders(Bill bill){
    	return bill.getSubmittedOrders();
    }
    
    public Collection<BaseOrderItem> getAllItemsByOrder(Bill bill){
    	Collection<Order> orders = getSubmittedOrders(bill);
    	Collection<BaseOrderItem> allItemsTemp = new ArrayList<>();
    	Collection<BaseOrderItem> allItemsFinal = new CopyOnWriteArrayList<>();
    	for (Order order : orders) {
    		Iterator<BaseOrderItem> orderItemIterator = order.getOrderItems().iterator();
    		while(orderItemIterator.hasNext()){
    			BaseOrderItem orderItem = orderItemIterator.next();
    			allItemsTemp.add(orderItem);
    		}
		}
    	for (BaseOrderItem baseOrderItem : allItemsTemp) {
    		if(allItemsFinal.size() == 0){
    			allItemsFinal.add(baseOrderItem);    			
    		}else {
        		for (Iterator<BaseOrderItem> boi = allItemsFinal.iterator(); boi.hasNext(); ) {
        			
        			boolean add = false;
        			boolean next = true;

    				while(next){
    					
    					if(boi.hasNext()){
    						BaseOrderItem item = boi.next();
    						while(item.getMenuItem().getId().equals(baseOrderItem.getMenuItem().getId())){
    	        				int get = baseOrderItem.getQuantity();
    	        				baseOrderItem.setQuantity(item.getQuantity() + get);
    	        				next = false;
    	        				break;
    						}
    						
    					}else{
    						add = true;
    						next = false;
    					}
    					
    					
    				}
    				
        			if(add){
        				allItemsFinal.add(baseOrderItem);
        			}
    				
    			}
    		}

			
		}
    	return allItemsFinal;
    }
}
