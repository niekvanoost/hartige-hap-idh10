package edu.avans.hartigehap.service.impl;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import edu.avans.hartigehap.domain.StateException;
import edu.avans.hartigehap.repository.Observer;
import edu.avans.hartigehap.service.Subject;
import lombok.extern.slf4j.Slf4j;

@Service("Subject")
@Repository
@Transactional(rollbackFor = StateException.class)
public class SubjectImpl implements Subject{
    private ArrayList<Observer> observers = new ArrayList<>();
    //private String productName;
    private int orderAmount;
    private Observer observer;

    //@Autowired
    public SubjectImpl(){
    }
    
    @Override
    public void setMessageVars(int OrderAmount){
        this.orderAmount=OrderAmount;    
    	notifyObservers();
    }

    @Override
    public void registerObserver(Observer observer) {
        observers.add(observer);
    }
    @Override
    public void removeObserver(Observer observer) {
        observers.remove(observer);
       
    }

    @Override
    public void notifyObservers() {
        //System.out.println("-----------------New bid placed----------------");
        for (Observer ob : observers) {
            ob.update(this.orderAmount );
        }
    }
}