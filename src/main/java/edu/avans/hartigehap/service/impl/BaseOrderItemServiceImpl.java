package edu.avans.hartigehap.service.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Lists;

import edu.avans.hartigehap.domain.BaseOrderItem;
import edu.avans.hartigehap.domain.DiningTable;
import edu.avans.hartigehap.domain.MenuItem;
import edu.avans.hartigehap.domain.OrderItem;
import edu.avans.hartigehap.domain.OrderOption;
import edu.avans.hartigehap.domain.StateException;
import edu.avans.hartigehap.repository.BaseOrderItemRepository;
import edu.avans.hartigehap.repository.MenuItemRepository;
import edu.avans.hartigehap.repository.OrderOptionRepository;
import edu.avans.hartigehap.service.BaseOrderItemService;
import edu.avans.hartigehap.service.MenuItemService;
import lombok.extern.slf4j.Slf4j;

@Service("BaseOrderItemService")
@Repository
@Transactional(rollbackFor = StateException.class)
public class BaseOrderItemServiceImpl implements BaseOrderItemService{
	
	@Autowired
	private BaseOrderItemRepository baseOrderItemRepository;
    @Autowired
    private OrderOptionRepository orderOptionRepository;
    @Autowired
    private MenuItemRepository menuItemRepository;
	
	public void save(OrderOption orderOption){
		baseOrderItemRepository.save(orderOption);
	}
	
	public void saveItem(OrderItem orderItem){
		baseOrderItemRepository.save(orderItem);
	}
	
	public BaseOrderItem findOne(Long id){
		return baseOrderItemRepository.findOne(id);
	}
	
    public void addOrder(DiningTable diningTable, BaseOrderItem orderItem){
    	diningTable.getCurrentBill().getCurrentOrder().getOrderItems().add(orderItem);
    }
    
    public void removeOrder(DiningTable diningTable){
		Collection<BaseOrderItem> allOrderItems = diningTable.getCurrentBill().getCurrentOrder().getOrderItems();
		diningTable.getCurrentBill().getCurrentOrder().getOrderItems().removeAll(allOrderItems);
    }
    
    public void addOption(DiningTable diningTable, String menuItemName){
    	
    			// ID opvragen van orderItem of option waaraan ik de nieuwe option moet
    			// koppelen.

    			Long lastId = getLastBaseOrderItemID();

    			// ID is binnen dus nu OrderItem of OrderOption ophalen.

    			BaseOrderItem orderItem = getBaseOrderItem(lastId);

    			// Vanuit de view krijg ik een menuitem naam ( Topping ) mee, deze moet
    			// ik koppelen aan een object.

    			MenuItem menuItem = menuItemRepository.findOne(menuItemName);

    			// Alle gegevens zijn nu beschikbaar. Basis item // Topping en aantal.
    			// Hiermee kan ik een nieuwe OrderOption aanmaken.

    			OrderOption orderOption = new OrderOption(orderItem, menuItem, 1);

    			// OrderOption opslaan

    			baseOrderItemRepository.save(orderOption);

    			// Nu dient de laatste optie gekoppeld te worden aan de diningtable.
    			// Ik verwijder voor het koppelen de eerder gekoppelde optie, anders is
    			// het geen decorator meer.
    			// En daarna koppel ik hem.

    			removeOrder(diningTable);
    			addOrder(diningTable, orderOption);
    	
    }
    
	public void removeOption(DiningTable diningTable, String menuItemName) {

		removeOrder(diningTable);
		Long id = getLastOrderOptionIdByMeal(menuItemName);
		baseOrderItemRepository.delete(id);
		OrderOption orderOption = orderOptionRepository.findOne(getLastBaseOrderItemID());
		if (orderOption == null) {
			BaseOrderItem orderItem = baseOrderItemRepository.findOne(getLastBaseOrderItemID());
			addOrder(diningTable, orderItem);
		} else {
			addOrder(diningTable, orderOption);
		}
	}
    
    public Long getLastBaseOrderItemID(){
		Iterator<BaseOrderItem> orderIterator = baseOrderItemRepository.findAll().iterator();
		BaseOrderItem orderItem = orderIterator.next();
    	Collection<BaseOrderItem> optionsTempAll = new ArrayList<>();
    	optionsTempAll.add(orderItem);
    	List<Long> longId = new ArrayList<>();
		while (orderIterator.hasNext()) {
			orderItem = orderIterator.next();
			optionsTempAll.add(orderItem);
    	}
		for(BaseOrderItem boi : optionsTempAll){
			longId.add(boi.getId());
		}
		long id = Collections.max(longId);
		return id;
    }
    	
    public Long getLastOrderOptionIdByMeal(String menuItemName){
    	Iterator<BaseOrderItem> orderIterator = baseOrderItemRepository.findAll().iterator();
    	BaseOrderItem orderItem = orderIterator.next();
    	Collection<BaseOrderItem> optionsTemp = new ArrayList<>();
    	Collection<BaseOrderItem> optionsTempAll = new ArrayList<>();
    	List<Long> longId = new ArrayList<>();
    	while (orderIterator.hasNext()) {
			orderItem = orderIterator.next();
			optionsTempAll.add(orderItem);
    	}
    	for(BaseOrderItem boi : optionsTempAll){
    		if(boi.getMenuItem().getId().equals(menuItemName)){
    			optionsTemp.add(boi);
    			}
    	}
    	
		for(BaseOrderItem boi : optionsTemp){
			longId.add(boi.getId());
		}
		long id = Collections.max(longId);
		return id;
    }
    
    public BaseOrderItem getBaseOrderItem(Long id){
    	BaseOrderItem orderItem = baseOrderItemRepository.findOne(id);
    	return orderItem;
    }
    
    public Collection<BaseOrderItem> getAllBaseOrderItems(){
    	Iterator<BaseOrderItem> orderIterator = baseOrderItemRepository.findAll().iterator();
    	BaseOrderItem orderItem = orderIterator.next();
    	Collection<BaseOrderItem> optionsTempAll = new ArrayList<>();
    	optionsTempAll.add(orderItem);
    	while (orderIterator.hasNext()) {
			orderItem = orderIterator.next();
			optionsTempAll.add(orderItem);
    	}
    	return optionsTempAll;
    }
	

	
	


}
