package edu.avans.hartigehap.service.impl;

import java.util.List;
import java.util.ListIterator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import edu.avans.hartigehap.service.*;
import edu.avans.hartigehap.domain.*;
import edu.avans.hartigehap.repository.OrderOptionRepository;
import edu.avans.hartigehap.repository.OrderRepository;

@Service("orderOptionService")
@Repository
@Transactional(rollbackFor = StateException.class)
@Slf4j
public class OrderOptionServiceImpl implements OrderOptionService {

   
}
