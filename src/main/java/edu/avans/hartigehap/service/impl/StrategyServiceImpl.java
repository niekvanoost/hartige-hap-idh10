package edu.avans.hartigehap.service.impl;

import edu.avans.hartigehap.service.StrategyService;
import lombok.extern.slf4j.Slf4j;

import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import edu.avans.hartigehap.domain.*;

@Service("StrategyService")
@Repository
@Transactional(rollbackFor = StateException.class)
@Slf4j
public class StrategyServiceImpl implements StrategyService{
	public String pay(String payMethod, String bill){
		String link = null;
		if (payMethod.equals("paypal")){
			link = "redirect:/paypal/" + bill;
		}
		if (payMethod.equals("ideal")){
			link = "redirect:/ideal/" + bill;
		}
		if (payMethod.equals("creditcard")){
			link = "redirect:/creditcard/" + bill;
		}
		return link;
	}
}

