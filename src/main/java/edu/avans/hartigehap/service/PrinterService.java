package edu.avans.hartigehap.service;

import java.io.FileNotFoundException;
import org.docx4j.openpackaging.exceptions.Docx4JException;
import com.itextpdf.text.DocumentException;


public interface PrinterService {

	void print(String filename) throws FileNotFoundException, DocumentException, Docx4JException;

}
