package edu.avans.hartigehap.service;


import edu.avans.hartigehap.domain.*;

public interface StrategyService {
	public String pay(String paymethod, String bill);
}
