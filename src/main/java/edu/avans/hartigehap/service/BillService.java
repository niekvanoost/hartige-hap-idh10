package edu.avans.hartigehap.service;

import java.util.Collection;
import java.util.List;

import edu.avans.hartigehap.domain.*;

public interface BillService {
    Bill findById(Long billId);

    void billHasBeenPaid(Bill bill) throws StateException;

    List<Bill> findSubmittedBillsForRestaurant(Restaurant restaurant);
    
    List<Bill> findPaidBillsForRestaurant(Restaurant restaurant);
    
    Order getOrder(Bill bill);
    
    Collection<Order> getSubmittedOrders(Bill bill);
    
    Collection<BaseOrderItem> getAllItemsByOrder(Bill bill);
}
