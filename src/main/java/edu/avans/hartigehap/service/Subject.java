package edu.avans.hartigehap.service;

import edu.avans.hartigehap.repository.Observer;

public interface Subject {
    public void registerObserver(Observer observer);
    public void removeObserver(Observer observer);
    public void notifyObservers();
    public void setMessageVars(int OrderAmount);
}