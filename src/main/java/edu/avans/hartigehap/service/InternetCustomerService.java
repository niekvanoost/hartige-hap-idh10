package edu.avans.hartigehap.service;

import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import edu.avans.hartigehap.domain.*;

public interface InternetCustomerService {
	
	void save(RealCustomer customer);
	
}
