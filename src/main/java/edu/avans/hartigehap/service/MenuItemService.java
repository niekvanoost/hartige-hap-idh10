package edu.avans.hartigehap.service;

import java.util.List;
import edu.avans.hartigehap.domain.*;

public interface MenuItemService {

    List<MenuItem> findAll();
    
    MenuItem findOne(String id);
}