package edu.avans.hartigehap.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import edu.avans.hartigehap.domain.*;

public interface InternetCustomerRepository extends PagingAndSortingRepository<RealCustomer, Long>{
	
	
}
