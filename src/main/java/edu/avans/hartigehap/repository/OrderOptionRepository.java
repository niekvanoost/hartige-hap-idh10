package edu.avans.hartigehap.repository;

import java.util.List;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.domain.*;
import edu.avans.hartigehap.domain.*;

public interface OrderOptionRepository extends PagingAndSortingRepository<OrderOption, Long> {


}
