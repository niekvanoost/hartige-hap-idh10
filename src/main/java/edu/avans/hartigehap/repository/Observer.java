package edu.avans.hartigehap.repository;

public interface Observer {
    public void update( int orderAmount);
  }