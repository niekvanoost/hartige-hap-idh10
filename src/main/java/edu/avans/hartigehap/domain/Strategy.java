package edu.avans.hartigehap.domain;

import edu.avans.hartigehap.service.StrategyService;

public class Strategy {
	StrategyService factory;
	String payMethod;

	public Strategy(String payMethod)
	{
		this.payMethod = payMethod;
	}

	public void setBehaviour(StrategyService factory)
	{
		this.factory = factory;
	}

	public StrategyService getBehaviour()
	{
		return factory;
	}

	public String getName() {
		return payMethod;
	}

	public void setName(String payMethod) {
		this.payMethod = payMethod;
	}
}
