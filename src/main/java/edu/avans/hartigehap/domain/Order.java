package edu.avans.hartigehap.domain;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import edu.avans.hartigehap.service.impl.DiningTableServiceImpl;
import edu.avans.hartigehap.web.controller.DiningTableController;

/**
 * 
 * @author Erco
 */
@Entity
@NamedQueries({ @NamedQuery(name = "Order.findSubmittedOrders", query = "SELECT o FROM Order o "
		+ "WHERE o.orderStatus = edu.avans.hartigehap.domain.Order$OrderStatus.SUBMITTED "
		+ "AND o.bill.diningTable.restaurant = :restaurant " + "ORDER BY o.submittedTime") })
// to prevent collision with MySql reserved keyword
@Table(name = "ORDERS")
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@id")
@Getter
@Setter
@ToString(callSuper = true, includeFieldNames = true, of = { "orderStatus", "orderItems" })
public class Order extends DomainObject {
	private static final long serialVersionUID = 1L;

	public enum OrderStatus {
		CREATED, SUBMITTED, PLANNED, PREPARED, SERVED
	}

	@Enumerated(EnumType.ORDINAL)
	// represented in database as integer
	private OrderStatus orderStatus;

	@Temporal(TemporalType.TIMESTAMP)
	private Date submittedTime;

	@Temporal(TemporalType.TIMESTAMP)
	private Date plannedTime;

	@Temporal(TemporalType.TIMESTAMP)
	private Date preparedTime;

	@Temporal(TemporalType.TIMESTAMP)
	private Date servedTime;

	// unidirectional one-to-many relationship.
	@OneToMany(cascade = javax.persistence.CascadeType.ALL)
	private Collection<BaseOrderItem> orderItems = new ArrayList<BaseOrderItem>();

	@ManyToOne()
	private Bill bill;

	public Order() {
		orderStatus = OrderStatus.CREATED;
	}

	/* business logic */

	@Transient
	public boolean isSubmittedOrSuccessiveState() {
		return orderStatus != OrderStatus.CREATED;
	}

	// transient annotation, because methods starting with are recognized by JPA
	// as properties
	@Transient
	public boolean isEmpty() {
		return orderItems.isEmpty();
	}

	public void addOrderItem(MenuItem menuItem) {
		Iterator<BaseOrderItem> orderItemIterator = orderItems.iterator();
		boolean found = false;
		while (orderItemIterator.hasNext()) {
			BaseOrderItem orderItem = orderItemIterator.next();
			if (orderItem.getMenuItem().equals(menuItem)) {
				orderItem.incrementQuantity();
				found = true;
				break;
			}
		}
		if (!found) {
			OrderItem orderItem = new OrderItem(menuItem, 1);
			orderItems.add(orderItem);
		}
	}

	public void addOrderOption(MenuItem menuItem, BaseOrderItem orderItem) {
		Iterator<BaseOrderItem> orderItemIterator = orderItems.iterator();

		while (orderItemIterator.hasNext()) {
			BaseOrderItem orderOptionA = orderItemIterator.next();
			if (orderOptionA.getMenuItem().getId().equals("Basis Pizza")) {
				newOption(orderItem, menuItem);

			} else {

				@SuppressWarnings("unchecked")
				Collection<OrderOption> variable = (Collection<OrderOption>) (Collection<?>) getOrderItems();
				while (variable.iterator().hasNext()) {
					OrderOption option = variable.iterator().next();
					addLoop(menuItem, option, orderItem);
					break;

				}

			}

		}
	}

	public void addLoop(MenuItem menuItem, OrderOption option, BaseOrderItem orderItem) {
		boolean loop1 = false;
		if (option.getOrderItem().getMenuItem().getId().equals("Basis Pizza")) {
			if (option.getMenuItem().getId().equals(menuItem.getId())) {
				option.incrementQuantity();
				loop1 = true;

			} else {
				newOption(orderItem, menuItem);
				loop1 = true;
			}
		}

		boolean found = false;
		if (!loop1) {
			OrderOption option2 = (OrderOption) option.getOrderItem();
			if (option2.getMenuItem().getId().equals(menuItem.getId())) {
				option2.incrementQuantity();
				found = true;

			}

			else if (option2.getOrderItem().getMenuItem().getId().equals("Basis Pizza")) {
				if (option2.getMenuItem().getId().equals(menuItem.getId())) {
					option2.incrementQuantity();
					found = true;

				} else if (option.getMenuItem().getId().equals(menuItem.getId())) {
					option.incrementQuantity();
					found = true;

				} else {
					newOption(orderItem, menuItem);
					found = true;
				}

			}

			boolean loop = false;

			if (!found) {
				OrderOption option3 = (OrderOption) option2.getOrderItem();
				if (option3.getOrderItem().getMenuItem().getId().equals("Basis Pizza")) {
					if (option3.getMenuItem().getId().equals(menuItem.getId())) {
						option3.incrementQuantity();
						loop = true;

					} else if (option2.getMenuItem().getId().equals(menuItem.getId())) {
						option2.incrementQuantity();
						loop = true;

					} else if (option.getMenuItem().getId().equals(menuItem.getId())) {
						option.incrementQuantity();
						loop = true;

					} else {
						newOption(orderItem, menuItem);
						loop = true;
					}

				}

				if (!loop) {
					OrderOption option4 = (OrderOption) option3.getOrderItem();
					if (option4.getOrderItem().getMenuItem().getId().equals("Basis Pizza")) {
						if (option4.getMenuItem().getId().equals(menuItem.getId())) {
							option4.incrementQuantity();
							loop = true;

						} else if (option3.getMenuItem().getId().equals(menuItem.getId())) {
							option3.incrementQuantity();
							loop = true;

						} else if (option2.getMenuItem().getId().equals(menuItem.getId())) {
							option2.incrementQuantity();
							loop = true;

						} else if (option.getMenuItem().getId().equals(menuItem.getId())) {
							option.incrementQuantity();
							loop = true;

						} else {
							newOption(orderItem, menuItem);
							loop = true;
						}

					}
				}
			}
		}

	}

	public void newOption(BaseOrderItem orderItem, MenuItem menuItem) {
		OrderOption orderOption = new OrderOption(orderItem, menuItem, 1);
		orderItems.removeAll(getOrderItems());
		orderItems.add(orderOption);
	}

	public BaseOrderItem getOrderItem(MenuItem menuItem) {
		Iterator<BaseOrderItem> orderItemIterator = orderItems.iterator();
		BaseOrderItem orderItem = orderItemIterator.next();
		while (orderItemIterator.hasNext()) {
			orderItem = orderItemIterator.next();
			if (orderItem.getMenuItem().getId().equals(menuItem.getId())) {
				orderItem.setMenuItem(menuItem);
			}
		}

		return orderItem;
	}

	public BaseOrderItem getLastOrderItem() {
		long id = orderItems.size();
		Iterator<BaseOrderItem> orderItemIterator = orderItems.iterator();
		BaseOrderItem orderItem = orderItemIterator.next();
		while (orderItemIterator.hasNext()) {
			orderItem = orderItemIterator.next();
			if (orderItem.getId().equals(id)) {
				orderItem.setId(id);
				;
			}
		}
		return orderItem;
	}

	public void deleteOrderItem(MenuItem menuItem, BaseOrderItem orderItemBase) {
		Iterator<BaseOrderItem> orderItemIterator = orderItems.iterator();

		while (orderItemIterator.hasNext()) {
			BaseOrderItem orderItem = orderItemIterator.next();
			if (orderItem.getMenuItem().equals(menuItem)) {
				if (orderItem.getQuantity() == 0) {
					// do nothing
					break;
				} else {
					orderItem.decrementQuantity();
					break;
				}
			} else {

				@SuppressWarnings("unchecked")
				Collection<OrderOption> variable = (Collection<OrderOption>) (Collection<?>) getOrderItems();
				while (variable.iterator().hasNext()) {
					OrderOption option = variable.iterator().next();
					OrderOption option2 = (OrderOption) option.getOrderItem();
					if (option2.getMenuItem().equals(menuItem)) {
						if (option2.getQuantity() == 0) {
							// do nothing
							break;
						} else {
							option2.decrementQuantity();
							break;
						}
					} else {
						OrderOption option3 = (OrderOption) option2.getOrderItem();
						if (option3.getMenuItem().equals(menuItem)) {
							if (option3.getQuantity() == 0) {
								// do nothing
								break;
							} else {
								option3.decrementQuantity();
								break;
							}
						} else {
							OrderOption option4 = (OrderOption) option3.getOrderItem();
							if (option4.getMenuItem().equals(menuItem)) {
								if (option4.getQuantity() == 0) {
									// do nothing
									break;
								} else {
									option4.decrementQuantity();
									break;
								}
							}

						}
					}
				}
			}
		}
	}

	public void submit() throws StateException {
		if (isEmpty()) {
			throw new StateException("not allowed to submit an empty order");
		}

		// this can only happen by directly invoking HTTP requests, so not via
		// GUI
		if (orderStatus != OrderStatus.CREATED) {
			throw new StateException("not allowed to submit an already submitted order");
		}
		submittedTime = new Date();
		orderStatus = OrderStatus.SUBMITTED;
	}

	public void plan() throws StateException {

		// this can only happen by directly invoking HTTP requests, so not via
		// GUI
		if (orderStatus != OrderStatus.SUBMITTED) {
			throw new StateException("not allowed to plan an order that is not in the submitted state");
		}

		plannedTime = new Date();
		orderStatus = OrderStatus.PLANNED;
	}

	public void prepared() throws StateException {

		// this can only happen by directly invoking HTTP requests, so not via
		// GUI
		if (orderStatus != OrderStatus.PLANNED) {
			throw new StateException(
					"not allowed to change order state to prepared, if it is not in the planned state");
		}

		preparedTime = new Date();
		orderStatus = OrderStatus.PREPARED;
	}

	public void served() throws StateException {

		// this can only happen by directly invoking HTTP requests, so not via
		// GUI
		if (orderStatus != OrderStatus.PREPARED) {
			throw new StateException("not allowed to change order state to served, if it is not in the prepared state");
		}

		servedTime = new Date();
		orderStatus = OrderStatus.SERVED;
	}

	@Transient
	public int getPrice() {
		int price = 0;
		Iterator<BaseOrderItem> orderItemIterator = orderItems.iterator();
		while (orderItemIterator.hasNext()) {
			price += orderItemIterator.next().getPrice();
		}
		return price;
	}

}
