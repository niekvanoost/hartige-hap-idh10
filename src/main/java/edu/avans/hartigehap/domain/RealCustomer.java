package edu.avans.hartigehap.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "OrderCustomer")
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@id")
@Getter
@Setter
public class RealCustomer {
	
	@ManyToOne()
	private Order order;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	public String firstName;
	public String lastName;
	public String address;
	public String zipCode;
	public String city;
	

	public RealCustomer(Order order, String firstName, String lastName, String address, String zipCode, String city) {
		this.order = order;
		this.firstName = firstName;
		this.lastName = lastName;
		this.address = address;
		this.zipCode = zipCode;
		this.city = city;
	}

}
