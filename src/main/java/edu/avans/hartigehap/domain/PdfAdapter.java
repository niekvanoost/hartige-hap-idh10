package edu.avans.hartigehap.domain;


import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;
import edu.avans.hartigehap.service.PrinterService;

@SuppressWarnings("unused")
public class PdfAdapter implements PrinterService{
	private Pdf pdf;
	
	public PdfAdapter(Pdf pdf){
		this.pdf = pdf;
	}

    /**
     * Creates a PDF document.
     * @param filename the path to the new PDF document
     * @throws    DocumentException 
     * @throws    FileNotFoundException 
     */

	
	public void print(String filename) throws FileNotFoundException, DocumentException {
		
			        // step 1
			        Document document = new Document();
			        // step 2
			        PdfWriter.getInstance(document, new FileOutputStream ("src/main/files/" + (filename) + ".pdf"));
			        // step 3
			        document.open();
			        // step 4
			        document.add(new Paragraph("Here should be the content for a bill"));
			        // step 5
			        document.close();
			    }
	}