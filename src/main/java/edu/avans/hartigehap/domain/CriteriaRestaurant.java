package edu.avans.hartigehap.domain;

import java.util.ArrayList;
import java.util.List;

import edu.avans.hartigehap.domain.Order.OrderStatus;

public class CriteriaRestaurant implements Criteria {

		@Override
		public List<Order> meetCriteria(List<Order> orders, Restaurant restaurant){
			List<Order> orderStatus = new ArrayList<Order>();
			for (Order order: orders){
				if(order.getBill().getDiningTable().getRestaurant().equals(restaurant)){
					orderStatus.add(order);
				}
			}
			return orderStatus;
		}
	}

