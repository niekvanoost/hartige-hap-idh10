package edu.avans.hartigehap.domain;


import edu.avans.hartigehap.repository.Observer;

public class PaidOrder implements Observer{

	String observerName;
    public PaidOrder(String ObserverName) {
        this.observerName = ObserverName;
    }
    @Override
    public void update(int OrderAmount){
    		System.out.println("Bingo! Er is weer geld binnen! Een totaal van "+OrderAmount+" euro.");
    }
    
    @Override
    public String toString(){
        return observerName;
    }

}