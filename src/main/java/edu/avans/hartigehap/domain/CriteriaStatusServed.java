package edu.avans.hartigehap.domain;
import java.util.ArrayList;
import java.util.List;

import edu.avans.hartigehap.domain.Order.OrderStatus;

public class CriteriaStatusServed implements Criteria {

	@Override
	public List<Order> meetCriteria(List<Order> orders, Restaurant restaurant){
		List<Order> orderStatus = new ArrayList<Order>();
		for (Order order: orders){
			if(order.getOrderStatus().equals(OrderStatus.SERVED)){
				orderStatus.add(order);
			}
		}
		return orderStatus;
	}
}