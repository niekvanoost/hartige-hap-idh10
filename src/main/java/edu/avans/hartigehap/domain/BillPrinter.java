package edu.avans.hartigehap.domain;

import java.util.Collection;

public abstract class BillPrinter {
	
	private Bill bill;
	private Customer customer;
	
	final void templateOrder() {
		
		getBillContent();
		getCustomerData();
		print(null);
	}
	
	
	final void getBillContent () {
	int price = bill.getPriceAllOrders();
	Collection<Order> orders = bill.getOrders();
		
	}	
	
	final void getCustomerData() {
		String name = customer.getFirstName();
		String lastname= customer.getLastName();
		
	}
	
	abstract void print(String filename) ;

}
