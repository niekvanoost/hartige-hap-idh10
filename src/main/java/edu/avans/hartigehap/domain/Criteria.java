package edu.avans.hartigehap.domain;

import java.util.List;

public interface Criteria{
	
	public List<Order> meetCriteria(List<Order> orders, Restaurant restaurant);	
	

}
