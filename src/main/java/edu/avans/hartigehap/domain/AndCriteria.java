package edu.avans.hartigehap.domain;

import java.util.List;

public class AndCriteria implements Criteria {

  private Criteria criteria;
  private Criteria otherCriteria;

  public AndCriteria(Criteria criteria, Criteria otherCriteria) {
    this.criteria = criteria;
    this.otherCriteria = otherCriteria;
  }

  @Override
  public List<Order> meetCriteria(List<Order> orders, Restaurant restaurant) {
    List<Order> firstOrderCriteria = criteria.meetCriteria(orders, restaurant);
    return otherCriteria.meetCriteria(firstOrderCriteria, restaurant);
  }
}
