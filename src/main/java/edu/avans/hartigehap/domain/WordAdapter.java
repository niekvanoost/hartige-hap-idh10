package edu.avans.hartigehap.domain;

import java.io.FileNotFoundException;

import org.docx4j.openpackaging.exceptions.Docx4JException;
import org.docx4j.openpackaging.exceptions.InvalidFormatException;
import org.docx4j.openpackaging.packages.WordprocessingMLPackage;

import edu.avans.hartigehap.service.PrinterService;

@SuppressWarnings("unused")
public class WordAdapter implements PrinterService{
	private Word word;
	
	public WordAdapter(Word word){
		this.word = word;
	}
	
	
	public void print(String filename) throws FileNotFoundException, Docx4JException {
		
		WordprocessingMLPackage wordMLPackage = WordprocessingMLPackage.createPackage();
		wordMLPackage.getMainDocumentPart().addStyledParagraphOfText("Title", "Hello Word!");
		wordMLPackage.getMainDocumentPart().addStyledParagraphOfText("Subtitle",
		"This is a subtitle!");
		wordMLPackage.save(new java.io.File("src/main/files/" + (filename) + ".docx"));
	}
}