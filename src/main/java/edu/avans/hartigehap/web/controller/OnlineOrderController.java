package edu.avans.hartigehap.web.controller;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import edu.avans.hartigehap.domain.BaseOrderItem;
import edu.avans.hartigehap.domain.DiningTable;
import edu.avans.hartigehap.domain.MenuItem;
import edu.avans.hartigehap.domain.OrderItem;
import edu.avans.hartigehap.domain.OrderOption;
import edu.avans.hartigehap.domain.Restaurant;
import edu.avans.hartigehap.repository.BaseOrderItemRepository;
import edu.avans.hartigehap.repository.MenuItemRepository;
import edu.avans.hartigehap.repository.OrderOptionRepository;
import edu.avans.hartigehap.repository.OrderRepository;
import edu.avans.hartigehap.service.BaseOrderItemService;
import edu.avans.hartigehap.service.DiningTableService;
import edu.avans.hartigehap.service.MenuItemService;
import edu.avans.hartigehap.service.RestaurantService;

@Controller
public class OnlineOrderController {

	final Logger logger = LoggerFactory.getLogger(DiningTableController.class);

	@Autowired
	private RestaurantService restaurantService;
	@Autowired
	private DiningTableService diningTableService;
	@Autowired
	private BaseOrderItemService baseOrderItemService;

	@RequestMapping(value = "/onlineOrderHome/{diningTableId}", method = RequestMethod.GET)
	public String showTable(@PathVariable("diningTableId") String diningTableId, Model uiModel) {
		logger.info("diningTable = " + diningTableId);

		Collection<Restaurant> restaurants = restaurantService.findAll();
		uiModel.addAttribute("restaurants", restaurants);
		DiningTable diningTable = diningTableService.fetchWarmedUp(Long.valueOf(diningTableId));
		uiModel.addAttribute("diningTable", diningTable);
		Restaurant restaurant = restaurantService.fetchWarmedUp(diningTable.getRestaurant().getId());
		uiModel.addAttribute("restaurant", restaurant);

		addBaseMenu("Basis Pizza", diningTable);
		
		Collection<BaseOrderItem> baseOrderItems = baseOrderItemService.getAllBaseOrderItems();
		uiModel.addAttribute("orders", baseOrderItems);
		
		String text = new String();
		
		for (BaseOrderItem baseOrderItem : baseOrderItems) {
			text = baseOrderItem.description();
		}
		
		uiModel.addAttribute("harry", text);
		
		return "hartigehap/onlineOrderHome";
	}

	@RequestMapping(value = "/onlineOrderHome/{diningTableId}/menuItems", method = RequestMethod.POST)
	public String addMenuOption(@PathVariable("diningTableId") String diningTableId, @RequestParam String menuItemName,
			Model uiModel) {

		DiningTable diningTable = diningTableService.fetchWarmedUp(Long.valueOf(diningTableId));
		uiModel.addAttribute("diningTable", diningTable);

		diningTableService.addOrderOption(diningTable,menuItemName);
			
		return "redirect:/onlineOrderHome/" + diningTableId;
	}

	@RequestMapping(value = "/onlineOrderHome/{diningTableId}/menuItems/{menuItemName}", method = RequestMethod.DELETE)
	public String deleteMenuItem(@PathVariable("diningTableId") String diningTableId,
			@PathVariable("menuItemName") String menuItemName, Model uiModel) {

		DiningTable diningTable = diningTableService.fetchWarmedUp(Long.valueOf(diningTableId));
		uiModel.addAttribute("diningTable", diningTable);

		diningTableService.deleteOrderItem(diningTable, menuItemName);


		return "redirect:/onlineOrderHome/" + diningTableId;
	}

	public void addBaseMenu(String menuItemName, DiningTable diningTable) {

		if (diningTable.getCurrentBill().getCurrentOrder().getPrice() < 1) {
			diningTableService.addOrderItem(diningTable, menuItemName);
		}

	}

}
