package edu.avans.hartigehap.web.controller;

import java.util.Collection;
import java.util.List;
import java.util.Locale;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import edu.avans.hartigehap.domain.*;
import edu.avans.hartigehap.domain.Bill.BillStatus;
import edu.avans.hartigehap.domain.Order.OrderStatus;
import edu.avans.hartigehap.repository.Observer;
import edu.avans.hartigehap.service.*;
import edu.avans.hartigehap.service.impl.SubjectImpl;
import edu.avans.hartigehap.web.form.Message;

@Controller
@PreAuthorize("hasRole('ROLE_EMPLOYEE')")
@Slf4j
public class WaiterController {

	@Autowired
	private MessageSource messageSource;
	@Autowired
	private RestaurantService restaurantService;
	@Autowired
	private BillService billService;
	@Autowired
	private StrategyService strategyService;
	@Autowired
	private OrderService orderService;

	@RequestMapping(value = "/restaurants/{restaurantName}/waiter", method = RequestMethod.GET)
	public String showWaiter(@PathVariable("restaurantName") String restaurantName, Model uiModel) {

		// warmup stuff
		Collection<Restaurant> restaurants = restaurantService.findAll();
		uiModel.addAttribute("restaurants", restaurants);
		Restaurant restaurant = restaurantService.fetchWarmedUp(restaurantName);
		uiModel.addAttribute("restaurant", restaurant);

		// Hier alle orders ophalen
		List<Order> allorders = orderService.findAll();
		Criteria orderStatus = new CriteriaStatusPrepared();
		Criteria orderRestaurant = new CriteriaRestaurant();
		Criteria preparedOrdersForRestaurant = new AndCriteria(orderStatus, orderRestaurant);

		List<Order> allPreparedOrders = preparedOrdersForRestaurant.meetCriteria(allorders, restaurant);

		// List<Order> allPreparedOrders =
		// orderService.findPreparedOrdersForRestaurant(restaurant);
		uiModel.addAttribute("allPreparedOrders", allPreparedOrders);

		List<Bill> allSubmittedBills = billService.findSubmittedBillsForRestaurant(restaurant);
		uiModel.addAttribute("allSubmittedBills", allSubmittedBills);

		List<Bill> allPaidBills = billService.findPaidBillsForRestaurant(restaurant);
		uiModel.addAttribute("allPaidBills", allPaidBills);

		return "hartigehap/waiter";
	}

	@RequestMapping(value = "/waiter/orders/{orderId}", method = RequestMethod.GET)
	public String showOrderInWaiter(@PathVariable("orderId") String orderId, Model uiModel, Locale locale) {

		// warmup stuff
		Order order = warmupRestaurantByOrder(orderId, uiModel);
		Restaurant resto = order.getBill().getDiningTable().getRestaurant();

		List<Order> allPreparedOrders = orderService.findPreparedOrdersForRestaurant(resto);
		uiModel.addAttribute("allPreparedOrders", allPreparedOrders);

		List<Bill> allSubmittedBills = billService.findSubmittedBillsForRestaurant(resto);
		uiModel.addAttribute("allSubmittedBills", allSubmittedBills);

		List<Bill> allPaidBills = billService.findPaidBillsForRestaurant(resto);
		uiModel.addAttribute("allPaidBills", allPaidBills);
		uiModel.addAttribute("order", order);

		String orderContent = "";
		for (BaseOrderItem orderItem : order.getOrderItems()) {
			orderContent += orderItem.getMenuItem().getId() + " (" + orderItem.getQuantity() + "x)" + "; ";
		}

		uiModel.addAttribute("message", new Message("info",
				messageSource.getMessage("label_order_content", new Object[] {}, locale) + ": " + orderContent));

		return "hartigehap/waiter";
	}

	@RequestMapping(value = "/waiter/bills/{billId}", method = RequestMethod.GET)
	public String showBillInlistbillcontent(@PathVariable("billId") String billId, Model uiModel, Locale locale) {

		// warmup stuff
		Bill bill = warmupRestaurant(billId, uiModel);
		Restaurant resto = bill.getDiningTable().getRestaurant();

		List<Order> allPreparedOrders = orderService.findPreparedOrdersForRestaurant(resto);
		uiModel.addAttribute("allPreparedOrders", allPreparedOrders);

		List<Bill> allSubmittedBills = billService.findSubmittedBillsForRestaurant(resto);
		uiModel.addAttribute("allSubmittedBills", allSubmittedBills);

		List<Bill> allPaidBills = billService.findPaidBillsForRestaurant(resto);
		uiModel.addAttribute("allPaidBills", allPaidBills);

		uiModel.addAttribute("message",
				new Message("info",
						messageSource.getMessage("label_bill_amount", new Object[] {}, locale) + ": "
								+ +bill.getPriceAllOrders() + " "
								+ messageSource.getMessage("label_currency", new Object[] {}, locale)));

		return "hartigehap/waiter";
	}

	@RequestMapping(value = "/waiter/bills/print/{billId}", method = RequestMethod.POST)
	public String PrintBillContent(@PathVariable("billId") String billId, Model uiModel, Locale locale) {

		// construction of a bill to a printable layout

		// Hier kun je in je view overheen itereren om de prijzen + aantallen
		// per item te tonen.

		// warmup stuff
		Bill bill = warmupRestaurant(billId, uiModel);
		uiModel.addAttribute("bill", bill);
		uiModel.addAttribute("billTotal", bill.getPriceAllOrders());

		Collection<BaseOrderItem> items = billService.getAllItemsByOrder(bill);
		uiModel.addAttribute("losseItems", items);

		uiModel.addAttribute("message",
				new Message("info",
						messageSource.getMessage("label_bill_amount", new Object[] {}, locale) + ": "
								+ +bill.getPriceAllOrders() + " "
								+ messageSource.getMessage("label_currency", new Object[] {}, locale)));

		return "hartigehap/listbillcontent";
	}

	@RequestMapping(value = "/paymethods/{billId}", method = RequestMethod.GET)
	public String paymentStrategy(@PathVariable("billId") String billId, Model uiModel, Locale locale) {

		System.out.println("Hij komt bij paymentstrategy");
		Bill bill = warmupRestaurant(billId, uiModel);
		uiModel.addAttribute("CurrrentBill", bill);

		return "hartigehap/paymethods";
	}

	@RequestMapping(value = "/paymethods/{paymethod}/test/{billId}", method = RequestMethod.GET)
	public String paymentStrategyLogic(@PathVariable("paymethod") String paymethod,
			@PathVariable("billId") String billId, Model uiModel, Locale locale) {

		Bill bill = warmupRestaurant(billId, uiModel);

		uiModel.addAttribute("PayMethod", paymethod);
		uiModel.addAttribute("CurrentBill", bill);

		return strategyService.pay(paymethod, billId);
	}

	@RequestMapping(value = "/paypal/{billId}", method = RequestMethod.GET)
	public String paypal(@PathVariable("billId") String billId, Model uiModel, Locale locale) {
		Bill bill = warmupRestaurant(billId, uiModel);
		uiModel.addAttribute("CurrentBill", bill);
		return "hartigehap/paypal";
	}

	@RequestMapping(value = "/ideal/{billId}", method = RequestMethod.GET)
	public String ideal(@PathVariable("billId") String billId, Model uiModel, Locale locale) {
		Bill bill = warmupRestaurant(billId, uiModel);
		uiModel.addAttribute("CurrentBill", bill);
		return "hartigehap/ideal";
	}

	@RequestMapping(value = "/creditcard/{billId}", method = RequestMethod.GET)
	public String creditcard(@PathVariable("billId") String billId, Model uiModel, Locale locale) {
		Bill bill = warmupRestaurant(billId, uiModel);
		uiModel.addAttribute("CurrentBill", bill);
		return "hartigehap/creditcard";
	}

	@RequestMapping(value = "/waiter/orders/{orderId}", method = RequestMethod.PUT)
	public String receiveOrderEvent(@PathVariable("orderId") String orderId, @RequestParam String event,
			Model uiModel) {

		Order order = warmupRestaurantByOrder(orderId, uiModel);

		switch (event) {
		case "orderHasBeenServed":
			orderHasBeenServed(order);
			break;

		default:
			log.error("Internal error: event " + event + " not recognized");
			break;
		}

		return "redirect:/restaurants/" + order.getBill().getDiningTable().getRestaurant().getId() + "/waiter";
	}

	private void orderHasBeenServed(Order order) {
		try {
			orderService.orderServed(order);
		} catch (StateException e) {
			log.error("Internal error has occurred! Order " + Long.valueOf(order.getId())
					+ "has not been changed to served state!", e);
		}
	}

	@RequestMapping(value = "/waiter/bills/{billId}", method = RequestMethod.PUT)
	public String receiveBillEvent(@PathVariable("billId") String billId, @RequestParam String event, Model uiModel) {

		Bill bill = warmupRestaurant(billId, uiModel);

		switch (event) {
		case "billHasBeenPaid":
			billHasBeenPaid(bill);
			break;

		default:
			log.error("Internal error: event " + event + " not recognized");
			break;
		}

		return "redirect:/restaurants/" + bill.getDiningTable().getRestaurant().getId() + "/waiter";
	}

	private void billHasBeenPaid(Bill bill) {
		try {
			Subject product = new SubjectImpl();
			Observer kitchen = new PaidOrder("Kitchen");
			product.registerObserver(kitchen);
			Observer waiter = new PaidOrder("Waiter");
			product.registerObserver(waiter);
			product.setMessageVars(bill.getPriceAllOrders());
			billService.billHasBeenPaid(bill);

		} catch (StateException e) {
			log.error("Internal error has occurred! Order " + Long.valueOf(bill.getId())
					+ "has not been changed to served state!", e);
		}
	}

	private Order warmupRestaurantByOrder(String orderId, Model uiModel) {
		Order order = orderService.findById(Long.valueOf(orderId));
		Collection<Restaurant> restaurants = restaurantService.findAll();
		uiModel.addAttribute("restaurants", restaurants);
		Restaurant restaurant = restaurantService
				.fetchWarmedUp(order.getBill().getDiningTable().getRestaurant().getId());
		uiModel.addAttribute("restaurant", restaurant);
		return order;
	}

	private Bill warmupRestaurant(String billId, Model uiModel) {
		Bill bill = billService.findById(Long.valueOf(billId));
		Collection<Restaurant> restaurants = restaurantService.findAll();
		uiModel.addAttribute("restaurants", restaurants);
		Restaurant restaurant = restaurantService.fetchWarmedUp(bill.getDiningTable().getRestaurant().getId());
		uiModel.addAttribute("restaurant", restaurant);
		return bill;
	}
}
