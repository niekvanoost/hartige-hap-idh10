package edu.avans.hartigehap.web.controller;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import javax.persistence.EnumType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import edu.avans.hartigehap.domain.BaseOrderItem;
import edu.avans.hartigehap.domain.Bill;
import edu.avans.hartigehap.domain.Bill.BillStatus;
import edu.avans.hartigehap.domain.DiningTable;
import edu.avans.hartigehap.domain.EmptyBillException;
import edu.avans.hartigehap.domain.Order;
import edu.avans.hartigehap.domain.RealCustomer;
import edu.avans.hartigehap.domain.StateException;
import edu.avans.hartigehap.service.BaseOrderItemService;
import edu.avans.hartigehap.service.BillService;
import edu.avans.hartigehap.service.DiningTableService;
import edu.avans.hartigehap.service.InternetCustomerService;
import edu.avans.hartigehap.service.OrderService;
import edu.avans.hartigehap.web.form.Message;

@Controller
public class OrderOverviewController {
	
	final Logger logger = LoggerFactory.getLogger(DiningTableController.class);

    @Autowired
    private DiningTableService diningTableService;
    @Autowired
    private InternetCustomerService customerService;
	@Autowired
	private BaseOrderItemService baseOrderItemService;
	@Autowired
	private OrderService orderService;
	
	@RequestMapping(value = "/orderOverview/{diningTableId}", method = RequestMethod.GET)
	public String showTable(@PathVariable("diningTableId") String diningTableId, Model uiModel) {
		logger.info("diningTable = " + diningTableId);

		DiningTable diningTable = diningTableService.fetchWarmedUp(Long.valueOf(diningTableId));
		uiModel.addAttribute("diningTable", diningTable);	
		Collection<BaseOrderItem> baseOrderItems = baseOrderItemService.getAllBaseOrderItems();
		uiModel.addAttribute("orders", baseOrderItems);
		
		String text = new String();
		
		for (BaseOrderItem baseOrderItem : baseOrderItems) {
			text = baseOrderItem.description();
		}
		
		uiModel.addAttribute("harry", text);
		
		return "hartigehap/orderOverview";
	}
	
	@RequestMapping(value = "/orderOverview/inputCustomer/{diningTableId}", method = RequestMethod.GET)
	public String inputCustomer(@PathVariable("diningTableId") String diningTableId, Model uiModel) {
		logger.info("diningTable = " + diningTableId);

		DiningTable diningTable = diningTableService.fetchWarmedUp(Long.valueOf(diningTableId));
		uiModel.addAttribute("diningTable", diningTable);
		
		Collection<BaseOrderItem> baseOrderItems = baseOrderItemService.getAllBaseOrderItems();
		uiModel.addAttribute("orders", baseOrderItems);
				
		return "hartigehap/orderInputCustomer";
	}
	
	@RequestMapping(value = "/orderOverview/inputCustomer/{diningTableId}", method = RequestMethod.POST)
	public String resultCustomer(@PathVariable("diningTableId")String diningTableId, @RequestParam("firstNameCustomer")String firstNameCustomer, @RequestParam("secondNameCustomer")String secondNameCustomer,
			@RequestParam("addressCustomer")String addressCustomer, @RequestParam("zipCode")String zipCode, @RequestParam("city")String city, Model uiModel) {
		logger.info("diningTable = " + diningTableId);

		logger.info(firstNameCustomer);
		logger.info(secondNameCustomer);
		
		DiningTable diningTable = diningTableService.fetchWarmedUp(Long.valueOf(diningTableId));
		Order order = orderService.findById(diningTable.getCurrentBill().getCurrentOrder().getId());
		
		RealCustomer customer = new RealCustomer(order,firstNameCustomer, secondNameCustomer, addressCustomer, zipCode, city);
		customerService.save(customer);
		
		Collection<BaseOrderItem> baseOrderItems = baseOrderItemService.getAllBaseOrderItems();
		uiModel.addAttribute("orders", baseOrderItems);
		uiModel.addAttribute("customer", customer);
		uiModel.addAttribute("diningTable", diningTable);
		
       try {
            diningTableService.submitOrder(diningTable);
        } catch (StateException e) {
            
        }
        
        try {
            diningTableService.submitBill(diningTable);
        } catch (EmptyBillException e) {

        } catch (StateException e) {
            
        }
        
        Collection<Bill> bills = diningTable.getBills();
        List<Long> longId = new ArrayList<>();
        for (Bill bill : bills) {
        	if(bill.getBillStatus() == BillStatus.SUBMITTED)
        	longId.add(bill.getId());
		}
		long id = Collections.min(longId);
  
		return "redirect:/paymethods/" + id;
	}
	

	

}
